<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <meta charset="utf-8">
    <title>To do List</title>
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    <style type="text/css">
</style>
    <link href="<c:url value="/css/todo.css" />" rel="stylesheet">
    <script src="<c:url value="/js/todo.js" />"></script>
    <script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <!-- jQuery UI、TableSorter（外部プラグイン）をダウンロード -->
    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/start/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
    <link rel=”stylesheet” type=”text/css” href=”${pageContext.request.contextPath}/css/todo.css” />
  </head>
  <body>
    <div id="jquery-ui-dialog" title="編集">
      <div id="editting">
        <span class="editForm">
          <form:form modelAttribute="editForm" action="${pageContext.request.contextPath}/todo/edit">
            <form:hidden path="id" value="" />
            <dl>
              <dt>
                <form:label path="priority">Priority:</form:label>
              </dt>
              <dd>
                <form:select path="priority">
                  <option value="0">Very Important</option>
                  <option value="1">High</option>
                  <option value="2">Middle</option>
                  <option value="3">Low</option>
                  <option value="4">Very Low</option>
                </form:select>
              </dd>
              <dt>
                <form:label path="dueDate">Due Date:</form:label>
              </dt>
              <dd>
                <form:input type="date" path="dueDate" />
              </dd>
              <dt>
                <form:label path="title">Title:</form:label>
              </dt>
              <dd>
                <form:input path="title" />
              </dd>
              <dt>
                <form:label path="detail">Detail:</form:label>
              </dt>
              <dd>
                <form:textarea path="detail" />
              </dd>
            </dl>
            <input type="submit" >
          </form:form>
        </span>
      </div>
    </div>
    <input type="hidden" id="prePriority" value="${editForm.priority} " />
    <!--  -->
    <c:if test="${isEditErrorFlag == true}" >
      <div id="jquery-ui-dialog2" title="エラー" >
        <div id="editting" id="showEditError">
          <span class="editForm">
            <span class="error-message-list" >
              <c:out value="${errorMessage }" />
            </span>
            <br />
            <span class="error-message-list" >
              <c:out value="${errorMessage2 }" />
            </span>
            <form:form modelAttribute="editForm" action="${pageContext.request.contextPath}/todo/edit">
              <form:errors path="*" element="div" cssClass="error-message-list" />
              <form:hidden path="id" value="" />
              <dl>
                <dt>
                  <form:label path="priority">Priority:</form:label>
                </dt>
                <dd>
                  <form:select path="priority" >
                    <option value="0">Very Important</option>
                    <option value="1">High</option>
                    <option value="2" >Middle</option>
                    <option value="3">Low</option>
                    <option value="4">Very Low</option>
                  </form:select>
                </dd>
                <dt>
                  <form:label path="dueDate">Due Date:</form:label>
                </dt>
                <dd>
                  <form:input type="date" path="dueDate" />
                </dd>
                <dt>
                  <form:label path="title">Title:</form:label>
                </dt>
                <dd>
                  <form:input path="title" />
                </dd>
                <dt>
                  <form:label path="detail">Detail:</form:label>
                </dt>
                <dd>
                  <form:textarea path="detail" />
                </dd>
              </dl>
              <input type="submit">
            </form:form>
          </span>
        </div>
      </div>
    </c:if>
    <div class="wrapper">
      <h1>${message}</h1>
      <div class="todoForm">
        <form:form modelAttribute="todoForm" action="${pageContext.request.contextPath}/todo">
          <c:if test="${isEditErrorFlag != true}">
            <c:if test="${not empty errorMessage }">
              <span class="error-message-list" >
                <c:out value="${errorMessage }" />
              </span>
              <br />
              <span class="error-message-list" >
                <c:out value="${errorMessage2 }" />
              </span>
            </c:if>
          </c:if>
          <form:errors path="*" element="div" cssClass="error-message-list" />
          <dl>
            <form:label path="title">Please input your task or any things to do :</form:label>
            <br />
            <dt>
              <form:label path="priority">Priority:</form:label>
            </dt>
            <dd>
              <form:select path="priority">
                <option value="0">Very Important</option>
                <option value="1">High</option>
                <option value="2" selected>Middle</option>
                <option value="3">Low</option>
                <option value="4">Very Low</option>
              </form:select>
            </dd>
            <dt>
              <form:label path="dueDate">Due Date:</form:label>
            </dt>
            <dd>
              <form:input type="date" path="dueDate" value=" "/>
            </dd>
            <dt>
              <form:label path="title">Title:</form:label>
            </dt>
            <dd>
              <form:input path="title" />
            </dd>
            <dt>
              <form:label path="detail">Detail:</form:label>
            </dt>
            <dd>
              <form:textarea path="detail" />
            </dd>
          </dl>
          <input type="submit" class="submitButton">
        </form:form>
      </div>
      <span id="isShowCompetedTodo">
        <input type="checkbox" id="chk2" />
        <label for="chk2">終了したTodoを表示しない</label>
      </span>
      <span id="isShowUpdatedDate">
        <input type="checkbox" id="chk1" />
        <label for="chk1">更新日を表示しない</label>
      </span>
      <span id="isShowCompletedDate">
        <input type="checkbox" id="chk3" />
        <label for="chk3">完了日を表示しない</label>
      </span>
      <span id="isShowId">
        <input type="checkbox" id="chk4" />
        <label for="chk4">ID番号を表示しない</label>
      </span>
      <table border="1" class="tablesorter showEveryTodo" id="todoTable" >
        <thead>
          <tr>
          <th class="detail">詳細</th>
          <th class="id">ID番号</th>
          <th class="priority">優先度</th>
          <th class="dueDate">期限</th>
          <th class="title" >内容</th>
          <th class="completedDate">完了日</th>
          <th class="updatedDate">更新日</th>
          <th class="buttons">完了・編集・削除</th>
          </tr>
        </thead>
        <tbody>
          <!--  表示部分 -->
          <c:forEach items="${todos}" var="todo">
            <c:if test="${todo.isCompleted != 0}">
              <tr class="completedTodo">
            </c:if>
            <c:if test="${todo.isCompleted == 0 }">
              <tr class="task">
            </c:if>
            <td>
              <c:if test="${not empty todo.detail }">
                <span class="isShowDetail">+</span>
                <span class="showDetail">
                  <pre><samp><c:out value="${todo.detail}"></c:out></samp></pre>
                </span>
              </c:if>
            </td>
            <td class="id">
              <span class="todo">
                <c:out value="${todo.id}">
                </c:out>
              </span>
            </td>
            <td class="priority">
              <span class="todo">
                <c:choose>
                  <c:when test="${todo.priority == 0 }">
                    <span>
                      <span class="hide">0:</span>Very Important
                    </span>
                  </c:when>
                  <c:when test="${todo.priority == 1 }">
                    <span>
                      <span class="hide">1:</span>High
                    </span>
                  </c:when>
                  <c:when test="${todo.priority == 2 }">
                    <span>
                      <span class="hide">2:</span>Middle
                    </span>
                  </c:when>
                  <c:when test="${todo.priority == 3 }">
                    <span>
                      <span class="hide">3:</span>Low
                    </span>
                  </c:when>
                  <c:when test="${todo.priority == 4 }">
                    <span>
                      <span class="hide">4:</span>Very Low
                    </span>
                  </c:when>
                </c:choose>
              </span>
            </td>
            <td class="dueDate">
              <span class="todo">
                <c:out value="${todo.dueDate}">
                </c:out>
              </span>
            </td>
            <td class="title">
              <span class="todo ">
                <c:out value="${todo.title}">
                </c:out>
              </span>
            </td>
            <td class="completedDate">
              <span class="todo">
                <c:out value="${todo.completedDate}">
                </c:out>
              </span>
            </td>
            <td class="updatedDate">
              <span class="todo">
                <c:out value="${todo.updatedDate}">
                </c:out>
              </span>
            </td>
            <td>
              <!--  完了フォーム -->
              <form:form modelAttribute="todoForm" action="${pageContext.request.contextPath}/todo/complete" method="post">
                <form:hidden path="id" value="${todo.id}" />
                <c:if test="${todo.isCompleted== 0}">
                  <form:hidden path="isCompleted" value="1" />
                  <form:button class="finish">完</form:button>
                </c:if>
                <c:if test="${todo.isCompleted != 0}">
                  <form:hidden path="isCompleted" value="0" />
                  <form:button class="continue">続</form:button>
                </c:if>
              </form:form>
              <!--  編集フォーム -->
              <button class="edit jquery-ui-dialog-opener" value="${todo.id},${todo.priority},${todo.dueDate},${todo.title},${todo.detail}">編</button>
              <!-- 削除ボタン -->
              <form:form modelAttribute="todoForm" onSubmit="return deleteConfirm()" action="${pageContext.request.contextPath}/todo/delete" method="post">
                <form:hidden path="id" value="${todo.id}" />
                <form:button>削</form:button>
              </form:form>
            </td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
  </body>
</html>