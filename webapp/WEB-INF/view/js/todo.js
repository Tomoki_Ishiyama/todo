$(function() {
	$('.completedTodo').css('background', '#ccc');
	$(".showDetail").hide();
	$(".hide").hide();

	function deleteConfirm() {
		myRet = window.confirm("本当に削除します。よろしいですか？");
		if (myRet == true) {
			alert("削除しました。");
			return true;
		} else {
			alert("キャンセルされました。");
			return false;
		}
	}

	$(".isShowDetail").on("click", function() {
		// $(".showDetail").toggle("fast");
		// event.stopPropagation();
		// $(this).toggle("fast");
		// $(this).find(“span”)
		// $(this).find(".showDetail").toggle("fast"); //選択要素の子供にある時
		// $(this).next(".showDetail").toggle("fast");//すぐ直後（隣）にある
		// $(this).parent(".showDetail").toggle("fast");//親要素
		// $(this).parents().next(".showDetail").toggle("fast"); //親の隣（親の親の隣に追加？
		// $(this).parents().child(".showDetail").toggle("fast");
		// $('this').find(".showDetail").appendTo("$(.detailShowSpace).next");
		// $(this).parents().next(".showDetail").append($(this).next(".showDetail"));
		// $(this).parents().next(".showDetailPosition").append($(this).parents().next(".showDetail"));
		$(this).next(".showDetail").toggle("fast");

	});

	$(".edit").on("click", function() {
		$(this).next(".editForm").show("fast");
	});

	$('.edit').on('click', function() {
		var str = $(this).val();
		var arrValue = str.split(',');
		console.log(arrValue);

		$('#editting #id').val(arrValue[0]);
		$('#editting #priority').val(arrValue[1]);
		$('#editting #dueDate').val(arrValue[2]);
		$('#editting #title').val(arrValue[3]);
		$('#editting #detail').val(arrValue[4]);

	});

	$("isCompleted").addClass("changed");
	$("#showEditError").show();
});

$(function() {
	$('#isShowCompetedTodo').change(function() {
		$('.completedTodo').toggle();
	})

	$('#isShowUpdatedDate').change(function() {
		$('.updatedDate').toggle();
	})

	$('#isShowId').change(function() {
		$('.id').toggle();
	})

	$('#isShowCompletedDate').change(function() {
		$('.completedDate').toggle();
	})

})

jQuery(function() {
	jQuery('button', '.jquery-ui-button').button();
	jQuery('.jquery-ui-dialog-opener').click(function() {
		jQuery('#jquery-ui-dialog').dialog('open');
	});

	jQuery('#jquery-ui-dialog').dialog({
		autoOpen : false,
		width : 540,
		modal : true,
	});
});

jQuery(function() {

	jQuery('.jquery-ui-dialog-opener2').show();

	var prep = Number($('#prePriority').val());
	console.log(prep)
	$('#editting #priority').val(prep);

	jQuery('#jquery-ui-dialog2').dialog({
		autoOpen : true,
		width : 540,
		modal : true,

	});
});

$(document).ready(function() {
	$("#todoTable").tablesorter({
		headers : {
			0 : {
				sorter : false
			},
			4 : {
				sorter : false
			},
			7 : {
				sorter : false
			}
		}
	});
});

function checkDisp(obj, id) {
	if (obj.checked) {
		document.getElementById(id).style.display = "block";
	} else {
		document.getElementById(id).style.display = "none";
	}
}

function deleteConfirm() {
	myRet = window.confirm("本当に削除します。よろしいですか？");
	if (myRet == true) {
		alert("削除しました。");
		return true;
	} else {
		alert("キャンセルされました。");
		return false;
	}
}
