package jp.co.todo.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.todo.dto.TodoDto;
import jp.co.todo.entity.TodoEntity;
import jp.co.todo.form.TodoForm;
import jp.co.todo.mapper.TodoMapper;

@Service
public class TodoService {

	@Autowired
	private TodoMapper todoRepository;

	public void insert(TodoForm form) {
		TodoEntity entity = convertToEntity(form);
		todoRepository.createTodo(entity);
	}

	private TodoEntity convertToEntity(TodoForm form) {
		TodoEntity entity = new TodoEntity();
		BeanUtils.copyProperties(form, entity);
		return entity;
	}

	private List<TodoDto> convertToDto(List<TodoEntity> todoList) {
		List<TodoDto> resultList = new LinkedList<>();
		for (TodoEntity entity : todoList) {
			TodoDto dto = new TodoDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public List<TodoDto> getEveryTodo() {
		List<TodoEntity> todoList = todoRepository.getEveryTodo();
		List<TodoDto> resultList = convertToDto(todoList);
		return resultList;
	}

	public void delete(int todoId) {
		todoRepository.deleteTodo(todoId);
	}

	public void completedTodo(int completedId, int isCompleted) {
		//		String strDate = null;
		//		if (isCompleted != 0) {
		//			strDate = "CURRENT_TIMESTAMP";
		//		}
		//
		//		todoRepository.completedTodo(completedId, isCompleted, strDate);

		if (isCompleted != 0) {
			todoRepository.completedTodo(completedId, isCompleted);
		} else {
			todoRepository.continueTodo(completedId, isCompleted);
		}
	}

	public void editTodo(TodoForm form) {
		TodoEntity entity = convertToEntity(form);
		todoRepository.editTodo(entity);
	}

}
