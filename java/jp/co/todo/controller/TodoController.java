package jp.co.todo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todo.dto.TodoDto;
import jp.co.todo.form.TodoForm;
import jp.co.todo.service.TodoService;

@Controller
//@Import(TodoService.class)
public class TodoController {

	@Autowired
	private TodoService todoService;

	@RequestMapping(value = "/todo", method = RequestMethod.GET, produces = "text/plain;charset=utf-8")
	public String show(Model model) {

		TodoForm todoForm = new TodoForm();
		List<TodoDto> todos = todoService.getEveryTodo();

		model.addAttribute("todoForm", todoForm);
		model.addAttribute("message", "To Do List ");
		model.addAttribute("todos", todos);
		model.addAttribute("editForm", new TodoForm());
		//List<TodoDto> tasks = todoService.getAllTasks();

		//model.addAttribute("tasks", tasks);
		return "todo";
	}

	@RequestMapping(value = "/todo", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String insert(@Valid @ModelAttribute("todoForm") TodoForm todoForm, BindingResult result, Model model) {

		boolean isErrorFlag = false;
		if (result.hasErrors()) {
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			isErrorFlag = true;
		}

		if (todoForm.getDetail().matches("^[  |　|\\r\\n|\t]+$") || todoForm.getTitle().matches("^[  |　]+$")) {
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			model.addAttribute("errorMessage2", "半角全角スペース・改行のみの入力は不可です");
			isErrorFlag = true;
		}

		if (isErrorFlag) {
			List<TodoDto> todos = todoService.getEveryTodo();
			model.addAttribute("todoForm", todoForm);
			model.addAttribute("message", "To Do List ");
			model.addAttribute("todos", todos);
			model.addAttribute("editForm", new TodoForm());
			return "todo";
		}
		todoService.insert(todoForm);

		//todo のみだと入力フォームのみ表示される状態になり、おかしい
		//Todo のJSPに単純に飛んでそのJSPを実行するだけで、POSTにTodoリストの表示を渡す処理がない。
		return "redirect:./todo";
	}

	@RequestMapping(value = "/todo/delete", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String delete(@ModelAttribute("deleteTodo") TodoForm todoForm, BindingResult result, Model model) {

		int deletedId = todoForm.getId();
		todoService.delete(deletedId);
		//todo のみだと入力フォームのみ表示される状態になり、おかしい
		//Todo のJSPに単純に飛んでそのJSPを実行するだけで、POSTにTodoリストの表示を渡す処理がない。
		//redirect:./todo と書いていたことによりURLがToDo/todo/todoになっていた
		//考察 ./は/todoまでを表す→.がカレントを表すことから現在ディレクトりの直前までのパスを取得している？
		return "redirect:./";
	}

	@RequestMapping(value = "/todo/complete", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String completedTask(@ModelAttribute("completedTodo") TodoForm todoForm, BindingResult result, Model model) {

		todoService.completedTodo(todoForm.getId(), todoForm.getIsCompleted());

		return "redirect:./";
	}

	@RequestMapping(value = "/todo/edit", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String editTask(@Valid @ModelAttribute("editForm") TodoForm todoForm, BindingResult result, Model model) {

		boolean isEditErrorFlag = false;
		if (result.hasErrors()) {
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			isEditErrorFlag = true;

		}

		if (todoForm.getDetail().matches("^[  |　|\\r\\n||\t]+$") || todoForm.getTitle().matches("^[  |　]+$")) {
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			model.addAttribute("errorMessage2", "半角全角スペース・改行のみの入力は不可です");
			isEditErrorFlag = true;
		}

		if (isEditErrorFlag) {//異常終了
			List<TodoDto> todos = todoService.getEveryTodo();

			model.addAttribute("editForm", todoForm);//編集のフォームは入力値を保持
			model.addAttribute("todoForm", new TodoForm());//入力フォームは空で
			model.addAttribute("message", "To Do List ");
			model.addAttribute("isEditErrorFlag", isEditErrorFlag);
			model.addAttribute("todos", todos);
			return "./todo";
		}

		todoService.editTodo(todoForm);

		return "redirect:./";
	}

}
