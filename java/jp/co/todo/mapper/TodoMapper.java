package jp.co.todo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.todo.entity.TodoEntity;

public interface TodoMapper {

	void createTodo(TodoEntity todo);

	List<TodoEntity> getEveryTodo();

	void deleteTodo(int id);

	void completedTodo(@Param("id") int id, @Param("isCompleted") int isCompleted);
	//void completedTodo(@Param("id") int id,@Param("isCompleted") int isCompleted,@Param("strDate") String strDate);

	void continueTodo(@Param("id") int id, @Param("isCompleted") int isCompleted);

	void editTodo(TodoEntity todo);
}
